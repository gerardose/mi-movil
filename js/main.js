//LOGIN PAGE: Check if email input is focus
var myinputMail = document.getElementById('form_email');
var myBtnLog = document.getElementById('login_btn');
var mailAlert = document.getElementById('al_log_tx');
var pssAlert = document.getElementById('al_pss_tx');
var mailErroneo = document.getElementById('mail_incorrecto');
var mailInp = false
var pssInp = false
//Si hay sim pendiente se muetra la ventana modal
var simPendiente=1;
function changeLabel(){
    var myparentMail = myinputMail.parentNode;
    myparentMail.classList.add('focused');
}
function returnLabel(){
  var myparentMail = myinputMail.parentNode;
  var myValueMail = myinputMail.value;
  if (myValueMail == "") {
    myparentMail.classList.remove('focused');
    mailAlert.classList.add('show_alert');
    mailErroneo.style.display = 'none';
    mailInp = false
    if (mailInp == false || pssInp == false) {
      myBtnLog.classList.add('btn_disable');
    }
  }else {
    if (emailIsValid(myinputMail.value)) {
        myparentMail.classList.add('focused');
        mailAlert.classList.remove('show_alert');
        mailErroneo.style.display = 'none';
        mailInp = true
      if (mailInp == true && pssInp == true) {
        myBtnLog.classList.remove('btn_disable');
      }
    }else{
        mailAlert.classList.remove('show_alert');
        mailErroneo.style.display = 'block';
        mailInp = false
      if (mailInp == false || pssInp == false) {
        myBtnLog.classList.add('btn_disable');
      }
    }
  }
}

//LOGIN PAGE: Check if password input is focus
var myinputPass = document.getElementById('form_password');

function changeLabelPass(){
    var myparentPass = myinputPass.parentNode;
    myparentPass.classList.add('focused');
}
function returnLabelPass(){
  var myparentPass = myinputPass.parentNode;
  var myValuePass = myinputPass.value;
  if (myValuePass == "") {
    myparentPass.classList.remove('focused');
    pssAlert.classList.add('show_alert');
    pssInp = false
    if (mailInp == false || pssInp == false) {
      myBtnLog.classList.add('btn_disable');
    }
  }else {
    myparentPass.classList.add('focused');
    pssAlert.classList.remove('show_alert');
    pssInp = true
    if (mailInp == true && pssInp == true) {
      myBtnLog.classList.remove('btn_disable');
    }
  }
}

//LOGIN PAGE: VALIDAR EMAIL
function emailIsValid (email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}


//RECOVER PASSWORD PAGE: Check if email input is focus
var myrecoverPss = document.getElementById('recover_password');
var myrecoverBtn = document.getElementById('sendMail_btn');

function changeRecoverLabel(){
    var myparentRecover = myrecoverPss.parentNode;
    myparentRecover.classList.add('focused');
}
function returnRecoverLabel(){
  var myparentRecover = myrecoverPss.parentNode;
  var myValueRecInp = myrecoverPss.value;
  if (myValueRecInp == "") {
    myparentRecover.classList.remove('focused');
    myrecoverBtn.classList.add('btn_disable');
    mailAlert.classList.add('show_alert');
    mailErroneo.style.display = 'none';
  }else {
    //myparentRecover.classList.add('focused');
    //myrecoverBtn.classList.remove('btn_disable');
    if (emailIsValid(myrecoverPss.value)) {
        myparentRecover.classList.add('focused');
        mailAlert.classList.remove('show_alert');
        mailErroneo.style.display = 'none';
        myrecoverBtn.classList.remove('btn_disable');
    }else{
        mailAlert.classList.remove('show_alert');
        mailErroneo.style.display = 'block';
        myrecoverBtn.classList.add('btn_disable');
    }
  }
}



//HOME Si el número de notificaciones es mayor que 9, muestra 9+
var numNotificaciones = document.getElementById('alertNumber');
if (numNotificaciones) {
  var numNotTxt = document.getElementById('alertNumber').innerText;
  var parseNumNot = parseInt(numNotTxt);
  if (parseNumNot > 9) {
    var nuevoContador = document.getElementById('alertNumber').innerText = '9+'
  }else if (parseNumNot == 0) {
    numNotificaciones.style.opacity = '0';
  }
}

//HOME: Si tiene saldo oculta texto -- Formatea el saldo como Divisa
var miSaldo = document.getElementById('mi-saldo');
if (miSaldo) {
  var miSaldoExistente = document.getElementById('mi-saldo').innerText;
}
var parseSaldo = parseFloat(miSaldoExistente);
const styleCurrency = { style: 'currency', currency: 'USD' };
const myNewFormat = new Intl.NumberFormat('en-US', styleCurrency);

if (miSaldoExistente) {
  var displaySaldo = document.getElementById('muestra-saldo').innerText = myNewFormat.format(parseSaldo);

  function miSaldoT(){
    var txtSaldo = document.getElementById('p-no-saldo');
    if (parseSaldo === 0) {
      txtSaldo.classList.remove('p-hidden');
    }else{
      txtSaldo.classList.add('p-hidden');
    }
  }
  miSaldoT();
}

//RESTABLECER PASSWORD
var newPasswordInput = document.getElementById('setNew_pss');

function validateNewPass(){
 var inputNewPassValue = newPasswordInput.value;
 var nameNewPassParent = newPasswordInput.parentNode;

 nameNewPassParent.classList.add('focused');

}
function returnLabelNewPass(){
  var inputNewPassValue = newPasswordInput.value;
  var nameNewPassParent = newPasswordInput.parentNode;

  if (inputNewPassValue == "") {
    nameNewPassParent.classList.remove('focused');
  }else {
    nameNewPassParent.classList.add('focused');
  }
}


var inputConfirmarNewPassword = document.getElementById('confirmNew_Pss');

function checkConfirmedPss(){
 var inputConfirmarNewPasswordValue = inputConfirmarNewPassword.value;
 var nameConfirmarPassParent = inputConfirmarNewPassword.parentNode;

 nameConfirmarPassParent.classList.add('focused');

}
function returnLabelConfirmedPss(){
  var inputConfirmarNewPasswordValue = inputConfirmarNewPassword.value;
  var nameConfirmarPassParent = inputConfirmarNewPassword.parentNode;
  var checkFirstPss = newPasswordInput.value;

  if (inputConfirmarNewPasswordValue == "") {
    nameConfirmarPassParent.classList.remove('focused');
  }else {
    nameConfirmarPassParent.classList.add('focused');
  }

  function coincidenciaPss(){
    var labelRecPss = document.getElementById('pss_confLb');

    if (checkFirstPss == inputConfirmarNewPasswordValue) {
      coincidenciaRecoverPss = true;
      labelRecPss.style.display = 'none';
    }else {
      coincidenciaRecoverPss = false;
      labelRecPss.style.display = 'block';
    }
  }

  coincidenciaPss();
}

//PONER NUEVO PASSWORD restablecer-password.html
var valInputRPass = document.getElementById('createR_pss');
var valInputConfRPass = document.getElementById('createR_conf_pss');
var btnR_disable = document.getElementById('setNewPssR_btn');

function enableRBtn(){
  if (isStrongR == true) {
    btnR_disable.classList.remove('btn_disable');
  }else if (isStrongR == false) {
    btnR_disable.classList.add('btn_disable');
  }
}

function validatePasswR(){
 var inputPassValueR = valInputRPass.value;
 var namePassParentR = valInputRPass.parentNode;

 namePassParentR.classList.add('focused');

}
function returnLabelPasswR(){
  var inputPassValueR = valInputRPass.value;
  var namePassParentR = valInputRPass.parentNode;

  if (inputPassValueR == "") {
    namePassParentR.classList.remove('focused');
  }else {
    namePassParentR.classList.add('focused');
  }
}
  //validar confirmacion
var valInputConfPassR = document.getElementById('createR_conf_pss');

  function validateConfPassR(){
   var inputConfPassValue = valInputConfPassR.value;
   var nameConfPassParent = valInputConfPassR.parentNode;

   nameConfPassParent.classList.add('focused');

  }
  function returnLabelConfPassR(){
    var inputConfPassValue = valInputConfPassR.value;
    var nameConfPassParent = valInputConfPassR.parentNode;
    var checkFirstPss = valInputRPass.value;

    if (inputConfPassValue == "") {
      nameConfPassParent.classList.remove('focused');
    }else {
      nameConfPassParent.classList.add('focused');
    }

    function coincidencia(){
      var labelPss = document.getElementById('pss_confR');
      var enableBtnCP = document.getElementById('setNewPss_btn');

      if (checkFirstPss == inputConfPassValue) {
        coincidenciaPss = true;
        labelPss.style.display = 'none';
        //enableBtnCP.classList.remove('btn_disable');
        enableRBtn()
      }else {
        coincidenciaPss = false;
        labelPss.style.display = 'block';
        btnR_disable.classList.add('btn_disable');
        //enableBtnCP.classList.add('btn_disable');
      }
    }

    coincidencia();
  }


//CREAR CUENTA: Validar inputs
var valInputName = document.getElementById('create_name');
var valInputFirstName = document.getElementById('create_ape_pat');
var valInputLastName = document.getElementById('create_ape_mat');
var valInputPhone = document.getElementById('create_phone');
var valInputMail = document.getElementById('create_mail');
var valInputPass = document.getElementById('create_pss');
var valInputConfPass = document.getElementById('create_conf_pss');
var coincidenciaPss = false;

var continueBtn = document.getElementById('register_Btn');


function validateName(){
  var inputNameValue = valInputName.value;
  var nameParent = valInputName.parentNode;

  nameParent.classList.add('focused');

}
 function returnLabelName(){
   var inputNameValue = valInputName.value;
   var nameParent = valInputName.parentNode;
   var regEmptyName = document.getElementById('regEmptyName');

   if (inputNameValue == "") {
     nameParent.classList.remove('focused');
     regEmptyName.style.display = 'block';
   }else {
     nameParent.classList.add('focused');
     regEmptyName.style.display = 'none';
   }
   checkInputs()
 }


function validateFName(){
 var inputFirstNameValue = valInputFirstName.value;
 var nameFNParent = valInputFirstName.parentNode;

 nameFNParent.classList.add('focused');

}
function returnLabelFName(){
  var inputFirstNameValue = valInputFirstName.value;
  var nameFNParent = valInputFirstName.parentNode;
  var registroApPat = document.getElementById('registroApPat');

  if (inputFirstNameValue == "") {
    nameFNParent.classList.remove('focused');
    registroApPat.style.display = 'block';
  }else {
    nameFNParent.classList.add('focused');
    registroApPat.style.display = 'none';
  }
  checkInputs()
}


function validateLName(){
  var inputLastNameValue = valInputLastName.value;
  var nameLNParent = valInputLastName.parentNode;

  nameLNParent.classList.add('focused');

}
 function returnLabelLName(){
   var inputLastNameValue = valInputLastName.value;
   var nameLNParent = valInputLastName.parentNode;

   if (inputLastNameValue == "") {
     nameLNParent.classList.remove('focused');
   }else {
     nameLNParent.classList.add('focused');
   }
   checkInputs()
 }


function validatePhone(){
 var inputPhoneValue = valInputPhone.value;
 var namePhoneParent = valInputPhone.parentNode;

 namePhoneParent.classList.add('focused');

}
function returnLabelPhone(){
  var inputPhoneValue = valInputPhone.value;
  var namePhoneParent = valInputPhone.parentNode;
  var emptyPhone = document.getElementById('emptyPhone');

  if (inputPhoneValue == "") {
    namePhoneParent.classList.remove('focused');
    emptyPhone.style.display = 'block';
  }else {
    namePhoneParent.classList.add('focused');
    emptyPhone.style.display = 'none';
  }
  checkInputs()
}


function validateMail(){
 var inputMailValue = valInputMail.value;
 var nameMailParent = valInputMail.parentNode;

 nameMailParent.classList.add('focused');

}
var isValidMail = false
function returnLabelMail(){
  var inputMailValue = valInputMail.value;
  var nameMailParent = valInputMail.parentNode;
  var alertMailEmpty =  document.getElementById('alertTxtMail');
  var mailNoValido = document.getElementById('alertMalNoValido');

  if (emailIsValid(valInputMail.value)) {
    nameMailParent.classList.add('focused');
    alertMailEmpty.style.display = "none";
    mailNoValido.style.display = "none";
    isValidMail = true
  }else if(!emailIsValid(valInputMail.value)) {
    if (inputMailValue == "") {
      nameMailParent.classList.remove('focused');
      alertMailEmpty.style.display = "block";
      mailNoValido.style.display = "none";
      isValidMail = false
    }else {
      nameMailParent.classList.add('focused');
      alertMailEmpty.style.display = "none";
      mailNoValido.style.display = "block";
      isValidMail = false
    }
  }
  checkInputs()
}



function validatePassw(){
 var inputPassValue = valInputPass.value;
 var namePassParent = valInputPass.parentNode;

 namePassParent.classList.add('focused');

}
function returnLabelPassw(){
  var inputPassValue = valInputPass.value;
  var namePassParent = valInputPass.parentNode;

  if (inputPassValue == "") {
    namePassParent.classList.remove('focused');
  }else {
    namePassParent.classList.add('focused');
  }
  checkInputs()
}


function validateConfPass(){
 var inputConfPassValue = valInputConfPass.value;
 var nameConfPassParent = valInputConfPass.parentNode;

 nameConfPassParent.classList.add('focused');

}
function returnLabelConfPass(){
  var inputConfPassValue = valInputConfPass.value;
  var nameConfPassParent = valInputConfPass.parentNode;
  var checkFirstPss = valInputPass.value;

  if (inputConfPassValue == "") {
    nameConfPassParent.classList.remove('focused');
  }else {
    nameConfPassParent.classList.add('focused');
  }

  function coincidencia(){
    var labelPss = document.getElementById('pss_conf');

    if (checkFirstPss == inputConfPassValue) {
      coincidenciaPss = true;
      labelPss.style.display = 'none';
    }else {
      coincidenciaPss = false;
      labelPss.style.display = 'block';
    }
  }

  coincidencia();
  checkInputs();
}




//CREAR CUENTA: Validación de password
var isStrong = false;
var timeout;
var timeUppercase;
var password = document.getElementById('create_pss');
var passwordR = document.getElementById('createR_pss');
var isStrongR = false;
var strengthBadge = document.getElementById('StrengthDisp')
var strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})')
var mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))')

function StrengthChecker(PasswordParameter){

        if(strongPassword.test(PasswordParameter)) {
            strengthBadge.style.backgroundColor = "#00ADBB"
            strengthBadge.textContent = 'Segura'
            isStrong = true;
            isStrongR = true;
        } else if(mediumPassword.test(PasswordParameter)){
            strengthBadge.style.backgroundColor = '#FF6B35'
            strengthBadge.textContent = 'Débil'
        } else{
            strengthBadge.style.backgroundColor = '#ff0000'
            strengthBadge.textContent = 'No segura'
        }
    }

    if (password) {
      password.addEventListener("input", () => {


          strengthBadge.style.display= 'block'
          clearTimeout(timeout);


          timeout = setTimeout(() => StrengthChecker(password.value), 500);


          if(password.value.length !== 0){
              strengthBadge.style.display != 'block'
          } else{
              strengthBadge.style.display = 'none'
          }
      });
    }

    if (passwordR) {
      passwordR.addEventListener("input", () => {


          strengthBadge.style.display= 'block'
          clearTimeout(timeout);


          timeout = setTimeout(() => StrengthChecker(passwordR.value), 500);


          if(passwordR.value.length !== 0){
              strengthBadge.style.display != 'block'
          } else{
              strengthBadge.style.display = 'none'
          }
      });
    }



//CREAR CUENTA: Habilitar botón de Continuar
function checkInputs(){
  if (valInputName.value.length == 0 || valInputFirstName.value.length == 0 || valInputPhone.value.length == 0 || isValidMail == false || valInputPass.value.length == 0 || valInputConfPass.value.length == 0 || isStrong == false || coincidenciaPss == false) {
    continueBtn.classList.add('btn_disable');
  }else if (valInputName.value.length !== 0 && valInputFirstName.value.length !== 0 && valInputPhone.value.length !== 0 && isValidMail == true && valInputPass.value.length !== 0 && valInputConfPass.value.length !== 0 && isStrong == true && coincidenciaPss == true){
    continueBtn.classList.remove('btn_disable');
  }
}


//MI CUENTA: Desplegar Menú "Mi cuenta"
var myAccount = document.getElementById('open_Mymenu');
var myAccountMenu = document.getElementById('wrapper_menu_miCuenta');

if (myAccount) {
  document.getElementById('open_Mymenu').onclick = function() {
      myAccountMenu.classList.add('active');
  }
  document.getElementById('closMenu').onclick = function() {
      myAccountMenu.classList.remove('active');
  }
  document.getElementById('list_verPerfil').onclick = function(){
      myAccountMenu.classList.add('showProfile');
  }
  document.getElementById('closPerfil').onclick = function() {
      myAccountMenu.classList.remove('showProfile');
  }
}


//Agregar saldo
var inputEmpty = false;
var confirmarButton = document.getElementById('unlockAdd_Saldo');
var my_monto = document.getElementById('my_monto');

//Portabilidad 2nd screen Next BTX
var unlkPortBtn = document.getElementById('portNextBtn');
function unlockPortabilidad(){
  var displayHiddnDiv = document.getElementById('hidden_div');
  if(document.querySelector('.inputsPortabilidad input[name="portabilidad_option"]:checked')){
    if (document.querySelector('.inputsPortabilidad input[name="portabilidad_option"]:checked').value == "yes") {
      displayHiddnDiv.style.display = 'block';
      unlkPortBtn.classList.remove('btn_disable');
    }else{
      unlkPortBtn.classList.remove('btn_disable');
      displayHiddnDiv.style.display = 'none';
    }

  }else {
    unlkPortBtn.classList.add('btn_disable');
  }
}

//Solicitar SIMs
var showMiSaldo = document.getElementById('show-mi-saldo');

if (showMiSaldo) {
  var miSaldoToTxt = document.getElementById('show-mi-saldo').innerText;
  var parseMiSaldo = parseFloat(miSaldoToTxt);
  const setCurrency = { style: 'currency', currency: 'USD' };
  const currencyFormat = new Intl.NumberFormat('en-US', setCurrency);
  var displaySaldo = document.getElementById('muestra-currency-saldo').innerText = currencyFormat.format(parseMiSaldo);
}

function askForSim(){
  var numberOfSimsRequested = document.getElementById('numberOfSims').value;
  var miSaldoToTxt = document.getElementById('show-mi-saldo').innerText;
  var requestButton = document.getElementById('unlockAskForSims_btn');
  var alertLabel = document.getElementById('dinamicLabel');
  var valorDeSim = 25;
  var montoTotal = numberOfSimsRequested * valorDeSim;
  var miSaldoValue = parseInt(miSaldoToTxt);

  //console.log('Pidio ' + numberOfSimsRequested + ' SIMS');
  //console.log('Tiene ' + miSaldoValue + ' de saldo');
  //console.log('Serían ' + montoTotal + ' totales');

  if (numberOfSimsRequested < 10 || numberOfSimsRequested > 100) {
    requestButton.classList.add('btn_disable');
    //console.log('Monto mínimo es de 10 Maximo es de 100');
    document.getElementById("dinamicLabel").innerHTML = "Mínimo 10 - Máximo 100";
    alertLabel.style.color = '#FFC02D';
  }else {
    if (miSaldoValue < montoTotal) {
      requestButton.classList.add('btn_disable');
      document.getElementById("dinamicLabel").innerHTML = "Saldo insuficiente";
      alertLabel.style.color = '#FFC02D';
      //console.log('NO te alcanza');
    }else if (miSaldoValue >= montoTotal) {
      requestButton.classList.remove('btn_disable');
      document.getElementById("dinamicLabel").innerHTML = "Saldo suficiente";
      alertLabel.style.color = '#28a745';
      //console.log('SI te alcanza');

    }
  }

}

/*ACTIVACION PORTABILIDAD*/
function selectThis(element){
  //element.classList.add('plan_selected');
  var planSelectedBtn = document.getElementById('btnPlanSelected');


  var el = document.querySelectorAll('.planContainer .white_module');
  for (let i = 0; i < el.length; i++) {
   el[i].onclick = function() {
     var c = 0;
     while (c < el.length) {
       el[c++].className = 'white_module mb-3 myOfferBck';
       planSelectedBtn.classList.remove('btn_disable');
     }
     el[i].className = 'white_module mb-3 myOfferBck plan_selected';
   };
 }
}
/*reposicion-de-simcard-costo*/
function seleccionaEsto(element){
  //element.classList.add('plan_selected');
  var planSelectedBtn = document.getElementById('btnPlanSelected');


  var el = document.querySelectorAll('.planContainer .white_module');
  for (let i = 0; i < el.length; i++) {
   el[i].onclick = function() {
     var c = 0;
     while (c < el.length) {
       el[c++].className = 'white_module mb-3 myOfferBck';
       numreposi_nextBtn.classList.remove('btn_disable');
     }
     el[i].className = 'white_module mb-3 myOfferBck plan_selected';
   };
 }
}

function showMyToolTip(){
  var showTooltip = document.getElementById('mini_icon');
  showTooltip.classList.toggle('showToolTip');
}

//VALIDAR INPUT IMEI
var imei = false;
var icc = false;
function validateIMEI(){
  var imeiInput = document.getElementById('ingresaIMEI');
  var imeiValue = imeiInput.value;
  if (imeiValue !== "") {
    validarEquipo.classList.remove('btn_disable');
    imei = true;
    if (imei == true && icc == true) {
      document.getElementById('icc_imei_nextBtn').classList.remove('btn_disable');
    }
  }else {
    validarEquipo.classList.add('btn_disable');
    imei = false;
    document.getElementById('icc_imei_nextBtn').classList.add('btn_disable');
  }
}
function validateEquipo(){
  var iccInput = document.getElementById('ingresaICC');
  var iccValue = iccInput.value;
  var validarICC = document.getElementById('validarICCbtn');
  if (iccValue !== "") {
    validarICC.classList.remove('btn_disable');
    icc = true;
    if (imei == true && icc == true) {
      document.getElementById('icc_imei_nextBtn').classList.remove('btn_disable');
    }
  }else {
    validarICC.classList.add('btn_disable');
    icc = false;
    document.getElementById('icc_imei_nextBtn').classList.add('btn_disable');
  }
}
function validateEquiponew(){
  var newiccInput = document.getElementById('ingresaICCnew');
  var newiccValue = newiccInput.value;
  var newvalidarICC = document.getElementById('validarICCbtnnew');
  if (newiccValue !== "") {
    newvalidarICC.classList.remove('btn_disable');
    icc = true;
    if (icc == true) {
      document.getElementById('icc_imei_nextBtnnew').classList.remove('btn_disable');
    }
  }else {
    newvalidarICC.classList.add('btn_disable');
    icc = false;
    document.getElementById('icc_imei_nextBtnnew').classList.add('btn_disable');
  }
}

//VALIDAR INPUT telefono reposicion simcard
var reposimcard = false;
function validateRepoSIMCard(){
  var reposimcardInput = document.getElementById('ingresaNumreposimcard');
  var reposimcardValue = reposimcardInput.value;
  if (reposimcardValue !== "") {
    validarnumsimcard.classList.remove('btn_disable');
    reposimcard = true;
    if (reposimcard == true) {
      document.getElementById('numreposi_nextBtn').classList.remove('btn_disable');
    }
  }else {
    validarnumsimcard.classList.add('btn_disable');
    reposimcard = false ;
    document.getElementById('numreposi_nextBtn').classList.add('btn_disable');
  }
}
//VALIDAR INPUT codigo 6 digitos reposicion simcard
var codigoreposimcard = false;
function validatecodigoRepoSIMCard(){
  var codigoreposimcardInput = document.getElementById('ingresacodigoreposimcard');
  var codigoreposimcardValue = codigoreposimcardInput.value;
  if (codigoreposimcardValue !== "") {
    validarcodigosimcard.classList.remove('btn_disable');
    codigoreposimcard = true;
    if (codigoreposimcard == true) {
      document.getElementById('codigoreposi_nextBtn').classList.remove('btn_disable');
    }
  }else {
    validarcodigosimcard.classList.add('btn_disable');
    codigoreposimcard = false || "123456";
    document.getElementById('codigoreposi_nextBtn').classList.add('btn_disable');
  }
}

//Recarga
function validatePhoneInp(){
  var recargaPhoneInp = document.getElementById('recargaIngresaPhone').value;
  var recargaSigBtn = document.getElementById('recargaSigBtn');
  if (recargaPhoneInp !== '') {
    recargaSigBtn.classList.remove('btn_disable');
  }else {
    recargaSigBtn.classList.add('btn_disable');
  }
}


//FAQs
var singleFaq = document.querySelector('.faq_single');
function show(element){
  element.classList.toggle('openFaq')
}

//Firma contratos
var termsAcceptedVar = false;
var aceptarContratoBtn = document.getElementById('aceptarContratoBtn');
var checkboxTerms = document.getElementById('termsAdnConditions')
var ciudadInput = document.getElementById('create_ciudad');

function termsAccepted(){
  var ciudadInputVal = ciudadInput.value;
  var estadoSelect = document.getElementById('estadoSelect');
  var estadoSelectVal = estadoSelect.value;

  if (document.getElementById("termsAdnConditions").checked == true) {
    termsAcceptedVar = true;
  }else {
    termsAcceptedVar = false;
  }

  if (document.getElementById("termsAdnConditions").checked == true && ciudadInputVal !== '' && estadoSelectVal !== 'empty') {
    aceptarContratoBtn.classList.remove('btn_disable');
  }else if (document.getElementById("termsAdnConditions").checked == false) {
    aceptarContratoBtn.classList.add('btn_disable');
  }
}

var estadoHasBeenSelected = false;
function returnEstado(){
  var estadoSeleccionado = document.getElementById('estadoSelect');
  var estadoSeleccionadoValue = estadoSeleccionado.value;

  if (estadoSeleccionadoValue !== 'empty') {
    estadoHasBeenSelected = true;
    var ciudadInputVal = ciudadInput.value;
    if (document.getElementById("termsAdnConditions").checked == true && ciudadInputVal !== '') {
      aceptarContratoBtn.classList.remove('btn_disable');
    }
  }else if (estadoSeleccionadoValue == 'empty') {
    aceptarContratoBtn.classList.add('btn_disable');
  }
}


var valCalleName = document.getElementById('create_calle');
var valNoExt = document.getElementById('noExterior');
var valNoInt = document.getElementById('noInterior');
var valColonia = document.getElementById('create_colonia');
var valCiudad = document.getElementById('create_ciudad');
var valCP = document.getElementById('myCP');
var valMunicipio = document.getElementById('create_municipio');

//calle
function validateCalle(){
  var inputCalleValue = valCalleName.value;
  var nameParent = valCalleName.parentNode;

  nameParent.classList.add('focused');

}
 function returnLabelCalle(){
   var inputCalleValue = valCalleName.value;
   var nameParent = valCalleName.parentNode;

   if (inputCalleValue == "") {
     nameParent.classList.remove('focused');
   }else {
     nameParent.classList.add('focused');
   }
 }
//colonia
function validateColonia(){
 var inputColoniaValue = valColonia.value;
 var nameParent = valColonia.parentNode;

 nameParent.classList.add('focused');

}
function returnLabelColonia(){
  var inputColoniaValue = valColonia.value;
  var nameParent = valColonia.parentNode;

  if (inputColoniaValue == "") {
    nameParent.classList.remove('focused');
  }else {
    nameParent.classList.add('focused');
  }
}
//No. exterior
 function validateNoExt(){
   var inputNoExtValue = valNoExt.value;
   var nameParent = valNoExt.parentNode;

   nameParent.classList.add('focused');

 }
function returnLabelNoExt(){
  var inputNoExtValue = valNoExt.value;
  var nameParent = valNoExt.parentNode;

  if (inputNoExtValue == "") {
    nameParent.classList.remove('focused');
  }else {
    nameParent.classList.add('focused');
  }
}
//No. interior
function validateNoInt(){
 var inputNoIntValue = valNoInt.value;
 var nameParent = valNoInt.parentNode;

 nameParent.classList.add('focused');

}
function returnLabelNoInt(){
  var inputNoIntValue = valNoInt.value;
  var nameParent = valNoInt.parentNode;

  if (inputNoIntValue == "") {
    nameParent.classList.remove('focused');
  }else {
    nameParent.classList.add('focused');
  }
}
//Codigo Postal
function valMyCP(){
 var inputCPValue = valCP.value;
 var nameParent = valCP.parentNode;

 nameParent.classList.add('focused');

}
function returnMyCP(){
  var inputCPValue = valCP.value;
  var nameParent = valCP.parentNode;

  if (inputCPValue == "") {
    nameParent.classList.remove('focused');
  }else {
    nameParent.classList.add('focused');
  }
}
//Delegacion / municipio
function validateMunicipio(){
 var inputMunicipioValue = valMunicipio.value;
 var nameParent = valMunicipio.parentNode;

 nameParent.classList.add('focused');

}
function returnLabelMunicipio(){
  var inputMunicipioValue = valMunicipio.value;
  var nameParent = valMunicipio.parentNode;

  if (inputMunicipioValue == "") {
    nameParent.classList.remove('focused');
  }else {
    nameParent.classList.add('focused');
  }
}
//Ciudad
var checkCiudad = false;
var checkEstado = false;

function validateCiudad(){
 var inputCiudadValue = valCiudad.value;
 var nameCiudadParent = valCiudad.parentNode;

 nameCiudadParent.classList.add('focused');

}
function returnLabelCiudad(){
  var inputCiudadValue = valCiudad.value;
  var nameCiudadParent = valCiudad.parentNode;
  var emptyCiudad = document.getElementById('emptyCiudad');

  if (inputCiudadValue == "") {
    nameCiudadParent.classList.remove('focused');
    emptyCiudad.style.display = 'block';
    aceptarContratoBtn.classList.add('btn_disable');
  }else {
    nameCiudadParent.classList.add('focused');
    emptyCiudad.style.display = 'none';
    checkCiudad = true;
    if (termsAcceptedVar == true && checkCiudad == true && estadoHasBeenSelected.value !== 'empty') {
      aceptarContratoBtn.classList.remove('btn_disable');
    }else if (termsAcceptedVar !== true || checkCiudad !== true || estadoHasBeenSelected !== true) {
      aceptarContratoBtn.classList.add('btn_disable');
    }
  }
}

function padLeadingZeros(num, size) {
  var s = num+"";
  while (s.length < size) s = "0" + s;
  return s;
}
// Date mis Movimientos
let from = document.getElementById("from");
let to = document.getElementById("to");
let today = new Date();
let dd = padLeadingZeros(today.getDate(),2);
let mm = padLeadingZeros(today.getMonth()+1,2); //January is 0!
let yyyy = padLeadingZeros(today.getFullYear(),2);

let today_menos3meses = new Date();
today_menos3meses.setMonth(today_menos3meses.getMonth()-3);
let dd_menos3meses = padLeadingZeros(today_menos3meses.getDate(),2);
let mm_menos3meses = padLeadingZeros(today_menos3meses.getMonth()+1,2);
let yyyy_menos3meses = padLeadingZeros(today_menos3meses.getFullYear(),2);
from?.setAttribute("min",""+yyyy_menos3meses+"-"+ mm_menos3meses+"-"+ dd_menos3meses);
to?.setAttribute("min",""+yyyy_menos3meses+"-"+ mm_menos3meses+"-"+ dd_menos3meses);
from?.setAttribute("max",""+yyyy+"-"+ mm+"-"+ dd);
to?.setAttribute("max",""+yyyy+"-"+ mm+"-"+ dd);
from?.addEventListener("change", function () {
  let input = this.value;
  let range = new Date(input);
  let ddRange = padLeadingZeros(range.getUTCDate(),2);
  let mmRange = padLeadingZeros(range.getUTCMonth() +1,2);
  let yyyyRange = padLeadingZeros(range.getFullYear(),2);
  to?.setAttribute("min", ""+yyyyRange+"-"+mmRange+"-"+ddRange);
});

// DATE MIS VENTAS// 
function padLeadingZeros2(num, size) {
  var s = num+"";
  while (s.length < size) s = "0" + s;
  return s;
}
let from2 = document.getElementById("from2");
let to2 = document.getElementById("to2");
let today2 = new Date();
let dd2 = padLeadingZeros2(today2.getDate(),2);
let mm2 = padLeadingZeros2(today2.getMonth()+1,2); //January is 0!
let yyyy2 = padLeadingZeros2(today2.getFullYear(),2);

let today_menos3meses2 = new Date();
today_menos3meses2.setMonth(today_menos3meses2.getMonth()-3);
let dd_menos3meses2 = padLeadingZeros2(today_menos3meses2.getDate(),2);
let mm_menos3meses2 = padLeadingZeros2(today_menos3meses2.getMonth()+1,2);
let yyyy_menos3meses2 = padLeadingZeros2(today_menos3meses2.getFullYear(),2);
from2?.setAttribute("min",""+yyyy_menos3meses2+"-"+ mm_menos3meses2+"-"+ dd_menos3meses2);
to2?.setAttribute("min",""+yyyy_menos3meses2+"-"+ mm_menos3meses2+"-"+ dd_menos3meses2);
from2?.setAttribute("max",""+yyyy2+"-"+ mm2+"-"+ dd2);
to2?.setAttribute("max",""+yyyy2+"-"+ mm2+"-"+ dd2);
from2?.addEventListener("change", function () {
  let input2 = this.value;
  let range2 = new Date(input2);
  let ddRange2 = padLeadingZeros2(range2.getUTCDate(),2);
  let mmRange2 = padLeadingZeros2(range2.getUTCMonth() +1,2);
  let yyyyRange2 = padLeadingZeros2(range2.getFullYear(),2);
  to2?.setAttribute("min", ""+yyyyRange2+"-"+mmRange2+"-"+ddRange2);
});

// DATE MIS solicitudes de simcards// 
function padLeadingZeros3(num, size) {
  var s = num+"";
  while (s.length < size) s = "0" + s;
  return s;
}
let from3 = document.getElementById("from3");
let to3 = document.getElementById("to3");
let today3 = new Date();
let dd3 = padLeadingZeros3(today3.getDate(),2);
let mm3 = padLeadingZeros3(today3.getMonth()+1,2); //January is 0!
let yyyy3 = padLeadingZeros3(today3.getFullYear(),2);

let today_menos3meses3 = new Date();
today_menos3meses3.setMonth(today_menos3meses3.getMonth()-3);
let dd_menos3meses3 = padLeadingZeros3(today_menos3meses3.getDate(),2);
let mm_menos3meses3 = padLeadingZeros3(today_menos3meses3.getMonth()+1,2);
let yyyy_menos3meses3 = padLeadingZeros3(today_menos3meses3.getFullYear(),2);
from3?.setAttribute("min",""+yyyy_menos3meses3+"-"+ mm_menos3meses3+"-"+ dd_menos3meses3);
to3?.setAttribute("min",""+yyyy_menos3meses3+"-"+ mm_menos3meses3+"-"+ dd_menos3meses3);
from3?.setAttribute("max",""+yyyy3+"-"+ mm3+"-"+ dd3);
to3?.setAttribute("max",""+yyyy3+"-"+ mm3+"-"+ dd3);
from3?.addEventListener("change", function () {
  let input3 = this.value;
  let range3 = new Date(input3);
  let ddRange3 = padLeadingZeros3(range3.getUTCDate(),2);
  let mmRange3= padLeadingZeros3(range3.getUTCMonth() +1,2);
  let yyyyRange3 = padLeadingZeros3(range3.getFullYear(),2);
  to3?.setAttribute("min", ""+yyyyRange3+"-"+mmRange3+"-"+ddRange3);
});

//Variables Mis Movimientos

/*var saldoI =document.querySelector("#saldoI");
saldoI.innerHTML =858;
var saldobI =document.querySelector("#saldobI");
saldobI.innerHTML =216;
var agregarS = document.querySelector("#agregarS");
agregarS.innerHTML =1000;
var gananciasP = document.querySelector("#gananciasP");
gananciasP.innerHTML =100;
var bloqueosS = document.querySelector("#bloqueosS");
bloqueosS.innerHTML =240;
var activacionP = document.querySelector("#activacionP");
activacionP.innerHTML =200;
var recargas = document.querySelector("#recargas");
recargas.innerHTML =200;
var desbloqueoaS = document.querySelector("#desbloqueoaS");
desbloqueoaS.innerHTML=200;
var costorepoSim = document.querySelector("#costorepoSim");
desbloqueoaS.innerHTML=200;
var saldoF = document.querySelector("#saldoF");
saldoF.innerHTML =658;
var saldobF = document.querySelector("#saldobF");
saldobF.innerHTML =192;*/


$( document )?.ready(function() {
  //recarga info de linea
  $("#recargaIngresaPhone").focusout(function() {
    if($(this).val().length>0){
      $("#validarEquipo").prop("disabled",false);
      $("#validarEquipo").removeClass("btn_disable");
    }else{
      $("#validarEquipo").prop("disabled",true);
      $("#validarEquipo").addClass("btn_disable")
    }
  });
  //agregar saldo
  $("#ingresaICC").focusout(function(){
    
    if($(this).val()>=500){
      $("#simInvalida").addClass("hide_txt");
    }else{
      $("#simInvalida").removeClass("hide_txt");
    }
  });
  

  $("#saldo_amount").focusout(function(){
    valida_saldo_amount();
  });
  $("#transfer_bbva,#transfer_bank,#practicaja_bbva").click(function(){
    valida_saldo_amount();
  })


  function valida_saldo_amount(){
      // mensaje Si el saldo es menor a $500 o mayor a $25,000 resaltar la lleyenda del asterizco con otro color.
    if($("#saldo_amount").val()<500 || $("#saldo_amount").val()>25000){
      $("#menos500Mas25").addClass("text-danger");
    }else{
      $("#menos500Mas25").removeClass("text-danger");
    }  
    //validacion sim valida/ sim invalida
    if($("#saldo_amount").val()>=500){
      $("#mensajeSuperiorA500").addClass("hide_txt");
      $("#unlockAdd_Saldo").removeClass("btn_disable");
    }else{
      $("#mensajeSuperiorA500").removeClass("hide_txt");
      $("#unlockAdd_Saldo").addClass("btn_disable");
    }  
  }
  //validacion imagen imei -> activaciin portabilidad equipo
  $("#validarEquipo").focusout(function(){
    
    if($(this).val()>=1){
      $("#noCarga").addClass("hide_txt");
    }else{
      $("#noCarga").removeClass("hide_txt");
    }  
  });
    //validacion imagen imei -> activacioin portabilidad equipo categoria solo valida con datos duros
document.querySelector("#validarEquipo")?.addEventListener("click",(e)=>{
  e.preventDefault();
  var imeiNoValid = document.querySelector("#ingresaIMEI").value; 
  console.log(imeiNoValid);
  if(imeiNoValid === "12345"){
    document.getElementById("imeiC").innerHTML = "IMEI Bloqueado";
  
  } else if (imeiNoValid === "11111"){
    document.getElementById("imeiC").innerHTML = "VoLTE";
  } else if(imeiNoValid === "22222"){
    document.getElementById("imeiC").innerHTML = "VozzApp";
  } else if(imeiNoValid === "33333"){
  document.getElementById("imeiC").innerHTML = "No probado";
  } else if(imeiNoValid === "44444"){
  document.getElementById("imeiC").innerHTML = "No Banda 28";
  } else if(imeiNoValid === "55555"){
  document.getElementById("imeiC").innerHTML = "Bloqueado";
  } 
})
//validacion numero telefono reposicion de sim datos duros
document.querySelector("#validarnumsimcard")?.addEventListener("click",(e)=>{
  e.preventDefault();
  var numNoValid = document.querySelector("#ingresaNumreposimcard").value; 
  console.log(numNoValid);
  if(numNoValid === "5555555555"){
    document.getElementById("numSimcard").innerHTML = "<p class='text-light'>Número Correcto</>";
  
  } else if(numNoValid === "55555"){
  document.getElementById("numSimcard").innerHTML = "<p class='text-danger'>Número incorrecto</>";
  } 
})
//validacion codigo 6 reposicion de sim datos duros
document.querySelector("#validarcodigosimcard")?.addEventListener("click",(e)=>{
  e.preventDefault();
  var codigoNoValid = document.querySelector("#ingresacodigoreposimcard").value; 
  console.log(codigoNoValid);
  if(codigoNoValid === "666666"){
    document.getElementById("codigoSimcard").innerHTML = "<p class='text-light'>Número Correcto</>";
  
  } else if(codigoNoValid === "123456"){
  document.getElementById("codigoSimcard").innerHTML = "<p class='text-danger'>Número incorrecto</>";
  } 
})
  //index
  $("#ojo").click(function(){
    $("#form_password").prop("type","text");
    $("#ojo_cerrado").show();
    $(this).hide();
  });
  $("#ojo_cerrado").click(function(){
    $("#form_password").prop("type","password");
    $("#ojo").show();
    $(this).hide();
  });
  //ingresa modaltrack solicitud sim
  if(simPendiente>0){
    $('#modalSimPendiente')?.modal('show');
    $(".close").click(function(){
      $('#modalSimPendiente')?.modal('toggle'); 
    });
  }
  //solicitar-cantidad-de-sims
  $("#numberOfSims").blur(function(){
    var muestraCurrencySaldo=$("#muestra-currency-saldo").text();
    muestraCurrencySaldo=muestraCurrencySaldo.replace("$","");
    var numberOfSims=$("#numberOfSims").val();
    var resta=muestraCurrencySaldo-(numberOfSims*25);
    $("#saldoDespuesOpracion").text(resta);
  });
  //mis movimientos alerta seleccionar fechas
  /*$("#from").blur(function(){
    if($("#from").val()=="" || $("#to").val()==""){
      $("#alerta_rango_fechas").show();
    }else{
      $("#alerta_rango_fechas").hide();
    }
  });
  $("#to").blur(function(){
    if($("#from").val()=="" || $("#to").val()==""){
      $("#alerta_rango_fechas").show();
    }else{
      $("#alerta_rango_fechas").hide();
    }
  });*/
  $("#alerta_rango_fechas").hide();
  $("#consulta_vacia").hide();
  $('#consultar_Calendarios').click(function(){
    var consulta = false;
    if($("#from").val()=="" || $("#to").val()==""){
      $("#alerta_rango_fechas").show();
    }else{
      $("#alerta_rango_fechas").hide();
    }
    if(consulta){
      $("#consulta_vacia").hide();
      $("#resumen_momimientos").hide();
    }else{
      $("#consulta_vacia").show();
      $("#resumen_momimientos").hide();
    }
  });

  $("#alerta_rango_fechas_ventas").hide();
  $("#consulta_vacia_ventas").hide();
  $('#consultar_Calendarios_Ventas').click(function(){
    var consulta = false;
    if($("#from2").val()=="" || $("#to2").val()==""){
      $("#alerta_rango_fechas_ventas").show();
    }else{
      $("#alerta_rango_fechas_ventas").hide();
    }
    if(consulta){
      $("#consulta_vacia_ventas").hide();
      $("#resumen_ventas").hide();

    }else{
      $("#consulta_vacia_ventas").show();
      $("#resumen_ventas").hide();

    }
  });
  // boton consultar mis solicitudes de simcards
  $("#alerta_rango_solisims").hide();
  $("#consulta_vaciasims").hide();
  $('#consultar_Solisimcards').click(function(){
    var consulta = false;
    if($("#from3").val()=="" || $("#to3").val()==""){
      $("#alerta_rango_solisims").show();
    }else{
      $("#alerta_rango_solisims").hide();
    }
    if(consulta){
      $("#consulta_vaciasims").hide();
      $("#resumen_solisims").show();
    }else{
      $("#consulta_vaciasims").show();
      $("#resumen_solisims").show();
    }
  });
// boton consultar mis solicitudes de reposicion de simcards
$('#consultar_Solireposimcards').click(function(){
  var consulta = false;
  if($("#ingresasolireposimcard").val()=="5555555555"){
    $("#solireposims").removeClass("hide");
  }
});
// boton estatus de portabilidad
$('#botonstatus').click(function(){
  var consulta = false;
  if($("#estatus_consulta").val()=="5555555555"){
    $("#track_estatus").removeClass("hide");
  }
  else{
    $("#track_estatuscancel").removeClass("hide");
  }
});
// campos reposicion simcard
  $('#validarnumsimcard').click(function(){
    var datosufinal = false;
    if($("#ingresaNumreposimcard").val()=="5555555555"){
      $("#datos_Userfinal").show();
    }else{
      $("#datos_Userfinal").hide();
    }
  });
  var create_name_valido=false;
  var create_ape_pat_valido=false;
  var create_mail_valido=false;

  $("#create_name").blur(function(){
    if($("#create_name").val().length==0){
      $("#regEmptyName").show();
      create_name_valido= false;
    }else{
      $("#regEmptyName").hide();
      create_name_valido= true;
    }
    activaBotonSiguiente();
  });
  $("#create_ape_pat").blur(function(){
    if($("#create_ape_pat").val().length==0){
      $("#registroApPat").show();
      create_ape_pat_valido= false;
    }else{
      $("#registroApPat").hide();
      create_ape_pat_valido= true;
    }
    activaBotonSiguiente();
  });
  $("#create_mail").blur(function(){
    if($("#create_mail").val().length==0){
      $("#alertTxtMail").show();
      create_mail_valido= false;
    }else{
      $("#alertTxtMail").hide();
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      if (testEmail.test($("#create_mail").val())){
        $("#alertMalNoValido").hide();
        create_mail_valido= true;
      }  
      else{
        $("#alertMalNoValido").show();
        create_mail_valido= false;
      }  
    }
    activaBotonSiguiente();
  });

  function activaBotonSiguiente(){
    if(create_name_valido==true && create_ape_pat_valido==true && create_mail_valido==true ){
      $("#numreposi_nextBtn").removeClass("btn_disable");
    }
    if(create_name_valido==false || create_ape_pat_valido==false || create_mail_valido==false ){
      $("#numreposi_nextBtn").addClass("btn_disable");
    }
    
  }

// boton validar activacion portabilidad confirmacion
var create_nameact_valido=false;
var create_ape_patact_valido=false;
var create_mailact_valido=false;

$("#create_nameact").blur(function(){
  if($("#create_nameact").val().length==0){
    $("#regEmptyNameact").show();
    create_nameact_valido= false;
  }else{
    $("#regEmptyNameact").hide();
    create_nameact_valido= true;
  }
  activaBotonSiguiente2();
});
$("#create_ape_patact").blur(function(){
  if($("#create_ape_patact").val().length==0){
    $("#registroApPatact").show();
    create_ape_patact_valido= false;
  }else{
    $("#registroApPatact").hide();
    create_ape_patact_valido= true;
  }
  activaBotonSiguiente2();
});
$("#create_mailact").blur(function(){
  if($("#create_mailact").val().length==0){
    $("#alertTxtMailact").show();
    create_mailact_valido= false;
  }else{
    $("#alertTxtMail").hide();
    var testEmailact = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if (testEmailact.test($("#create_mailact").val())){
      $("#alertMalNoValidoact").hide();
      create_mailact_valido= true;
    }  
    else{
      $("#alertMalNoValidoact").show();
      create_mailact_valido= false;
    }  
  }
  activaBotonSiguiente2();
});

function activaBotonSiguiente2(){
  if(create_nameact_valido==true && create_ape_patact_valido==true && create_mailact_valido==true ){
    $("#activa_nextBtn").removeClass("btn_disable");
  }
  if(create_nameact_valido==false || create_ape_patact_valido==false || create_mailact_valido==false ){
    $("#activa_nextBtn").addClass("btn_disable");
  }
  
}

});

//crear cuenta
$("#ojocc").click(function(){
  $("#create_pss").prop("type","text");
  $("#ojocc_cerrado").show();
  $(this).hide();
});
$("#ojocc_cerrado").click(function(){
  $("#create_pss").prop("type","password");
  $("#ojocc").show();
  $(this).hide();
});
//no coinciden laas contraseñas
$("#create_conf_pss").blur(function(){
  
  if($("#create_pss").val()!=$("#create_conf_pss").val()){
    $("#mensajeCoinciden").css("display","block");
  }else{
    $("#mensajeCoinciden").css("display","none");
  }
});
$("#ojocc2").click(function(){
  $("#create_conf_pss").prop("type","text");
  $("#ojocc2_cerrado").show();
  $(this).hide();
});
$("#ojocc2_cerrado").click(function(){
  $("#create_conf_pss").prop("type","password");
  $("#ojocc2").show();
  $(this).hide();
});

//restablecer password
$("#ojorp").click(function(){
  $("#createR_pss").prop("type","text");
  $("#ojorp_cerrado").show();
  $(this).hide();
});
$("#ojorp_cerrado").click(function(){
  $("#createR_pss").prop("type","password");
  $("#ojorp").show();
  $(this).hide();
});

$("#ojorp2").click(function(){
  $("#createR_conf_pss").prop("type","text");
  $("#ojorp2_cerrado").show();
  $(this).hide();
});
$("#ojorp2_cerrado").click(function(){
  $("#createR_conf_pss").prop("type","password");
  $("#ojorp2").show();
  $(this).hide();
});